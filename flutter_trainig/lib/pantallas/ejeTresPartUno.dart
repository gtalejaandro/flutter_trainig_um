import 'package:flutter/material.dart';

class EjeTresPartUno extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Center(
            child: Text(
              'SKA-P',
              style: TextStyle(
                fontFamily: 'Anton',
                fontSize: 40,
                color: Colors.red,
                letterSpacing: 4,
              ),
            ),
          ),
          actions: [
            IconButton(
              icon: Icon(
                Icons.star_border,
                color: Colors.red,
              ),
              onPressed: null,
            ),
          ],
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              flex: 5,
              child: SingleChildScrollView(
                child: Container(
                    width: MediaQuery.of(context).size.width / 1.111,
                    child: Column(
                      children: [
                        Divider(),
                        Container(
                          height: 400,
                          decoration: BoxDecoration(
                            color: Colors.red,
                            borderRadius: BorderRadius.circular(20.0),
                          ),
                          child: Center(
                            child: SizedBox(
                              width: 250,
                              height: 250,
                              child: CircleAvatar(
                                radius: 15.0,
                                backgroundImage: AssetImage('img/skap.jpeg'),
                              ),
                            ),
                          ),
                        ),
                        Divider(),
                        Text(
                          ' Una de las mas importantes bandas de ska punk del planeta.',
                          style: TextStyle(
                            fontSize: 25,
                            fontFamily: 'Anton',
                            color: Colors.red,
                          ),
                          textAlign: TextAlign.justify,
                        ),
                        Divider(),
                        Text(
                          'Ska-p nace como grupo en l994 entoces lo formaban:\nPulpul, a la voz principal y guitarra\nToni Escobar, guitarra y coros\nJulio, bajo\nKogote, teclados y coros\nPako, bateria\nTras unos meses de ensayos graban lo que es su primer cd. llamado ska-p, donde ya apuntaban las maneras en sus letras y música, que unos años después les harían muy populares, pero aun faltaba tiempo para eso, tocan en muchos lugares, por los gastos y poco más, (Galicia, Almeria, Madrid, etc..) entonces no eran conocidos apenas por lo que muchos de sus conciertos fueron para pequeñas cantidades de gente y casi ignorados, en áquel momento.',
                          style: TextStyle(
                            fontFamily: 'Teko',
                            fontSize: 20,
                            color: Colors.black,
                          ),
                          textAlign: TextAlign.justify,
                        )
                      ],
                    )),
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(60),
                    color: Color(0x29EB1555),
                    border: Border.all(
                      color: Colors.white,
                      width: 7,
                    )),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Text(
                      'DISCOGRAFIA',
                      style: TextStyle(
                          fontFamily: 'Teko',
                          fontWeight: FontWeight.bold,
                          fontSize: 22,
                          color: Colors.black,
                          letterSpacing: 2),
                    ),
                    FlatButton(
                      onPressed: () {},
                      textColor: Colors.black,
                      color: Colors.redAccent,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50),
                      ),
                      child: Text(
                        'DESCARGAR',
                        style: TextStyle(fontFamily: 'Teko', fontSize: 22),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ));
  }
}
/*
*    Container(

*
* */
