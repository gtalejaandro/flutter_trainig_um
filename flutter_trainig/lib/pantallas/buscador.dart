import 'package:flutter/material.dart';
import 'resultado.dart';

//TODO: Midifique si es neceario, la clase para que se manejen correctamente los estados requeridos.

class Buscador extends StatefulWidget {
  //TODO: Agregue las funcionalidades necesarias a este widget segun lo pedido
  @override
  _BuscadorState createState() => _BuscadorState();
}

class _BuscadorState extends State<Buscador> {
  bool switchvalue = true;
  int textfilevalue;
  TextEditingController _editingController = TextEditingController();

  void textField(String value) {
    setState(() {
      textfilevalue = int.parse(_editingController.text);
    });
  }

  void control(bool value) {
    if (switchvalue) {
      setState(() {
        switchvalue = false;
      });
    } else {
      setState(() {
        switchvalue = true;
      });
    }
  }

  Widget tipoDeBusqueda() {
    return Row(
      children: <Widget>[
        Spacer(
          flex: 2,
        ),
        Text(
          "Busqueda Simple ",
          style: TextStyle(fontFamily: 'Teko', fontSize: 20),
        ),
        Expanded(
          child: Switch(
            value: switchvalue,
            onChanged: control,
            activeColor: Colors.green,
            inactiveThumbColor: Colors.red,
          ),
        ),
        Spacer(
          flex: 2,
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Center(child: Text("Buscador")),
        ),
        body: Center(
          child: Column(
            children: <Widget>[
              Spacer(flex: 2),
              TextField(
                controller: _editingController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Introduce el numero de la imagen que buscas',
                  hintText: 'Ejemplo: 22028285',
                ),
                keyboardType: TextInputType.number,
                onSubmitted: textField,
              ),
              Spacer(
                flex: 1,
              ),
              Expanded(child: tipoDeBusqueda()),
              Spacer(
                flex: 1,
              ),
              RaisedButton(
                child: Text("Buscar"),
                onPressed: () {
                  print(
                      'Valor de switch : $switchvalue \n Valor del textfield: $textfilevalue');
                  if ((textfilevalue > 11) || (textfilevalue < 0)) {
                    return showDialog(
                        context: context,
                        builder: (context) {
                          return AlertDialog(
                            title: Text(
                              'YOU SHALL NOT PASS',
                              style: TextStyle(
                                  fontFamily: 'Teko',
                                  fontSize: 29,
                                  color: Colors.red),
                              textAlign: TextAlign.center,
                            ),
                            content: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  'El valor introducido no es valido. vuelve a intentarlo',
                                  style: TextStyle(
                                      fontFamily: 'Anton',
                                      fontSize: 19,
                                      color: Colors.white70),
                                  textAlign: TextAlign.center,
                                ),
                                RaisedButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                  color: Colors.blue,
                                  child: Text(
                                    'ACEPTAR',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 16, fontFamily: 'Teko'),
                                  ),
                                )
                              ],
                            ),
                          );
                        });
                  } else {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Resultado(
                                  switchvalue,
                                  textfilevalue,
                                )));
                  }
                },
              ),
              Spacer(
                flex: 1,
              ),
            ],
          ),
        ));
  }
}
