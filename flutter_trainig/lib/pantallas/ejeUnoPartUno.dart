import 'package:flutter/material.dart';
import 'ejeDosPartUno.dart';
import 'buscador.dart';

class EjeUnoPartUno extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text('Ejercicio 1 parte 1')),
      ),
      body: ListView(
        children: [
          SizedBox(
            height: 30,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 35),
            child: Container(
              width: 550,
              height: 90,
              child: FlatButton(
                shape: RoundedRectangleBorder(
                  side: BorderSide(color: Colors.purple),
                  borderRadius: BorderRadius.circular(5.3),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.ac_unit,
                      color: Colors.white,
                    ),
                    SizedBox(width: 17),
                    Text(
                      'FlatButton',
                      style: TextStyle(color: Colors.white),
                    )
                  ],
                ),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Buscador()));
                },
              ),
            ),
          ),
          Card(
            color: Color(0xFF0A0E32),
            shape: RoundedRectangleBorder(
              side: BorderSide(color: Colors.purple),
              borderRadius: BorderRadius.circular(5.3),
            ),
            child: ListTile(
              leading: Icon(
                Icons.accessibility,
                size: 50,
              ),
              title: Text(
                'Tile Text',
                style: TextStyle(fontSize: 20, letterSpacing: 2),
              ),
              subtitle: Text(
                'SubTile Text',
                style: TextStyle(fontSize: 20, letterSpacing: 2),
              ),
            ),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.purple,
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => EjeDosPartUno()));
        },
        tooltip: 'Increment',
        child: Icon(Icons.arrow_forward),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
