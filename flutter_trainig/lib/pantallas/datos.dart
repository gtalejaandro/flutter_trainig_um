class Datos {
  static List<String> imgBank = [
    'img/canarionegro.jpg', //0
    'img/daimon.jpg',
    'img/franklin.jpg',
    'img/ghostrider.jpg',
    'img/gwen.jpg',
    'img/ironfist.jpg',
    'img/logan.jpg',
    'img/lucifer.jpg',
    'img/nightcrawler.jpg',
    'img/redhulk.jpg',
    'img/spider.jpg',
    'img/thania.jpg'
  ];
  static List<String> nameChar = [
    '-Canario Negro',
    '-Daimon Hellstrome',
    '-Franklin Richards',
    '-Ghost Rider',
    '-Spider-Gwen',
    '-Iron Fist',
    '-Logan',
    '-Lucifer Morningstar',
    '-Nightcrawler',
    '-Red Hulk',
    '-Spiderman Fundacion Futuro',
    '-Thalia al Ghul'
  ];
  static List<String> originChar = [
    '-DC Comics',
    '-Marvel Comics',
    '-Marvel Comics',
    '-Marvel Comics',
    '-Marvel Comics',
    '-Marvel Comics',
    '-Marvel Comics',
    '-Vertigo Comics',
    '-Marvel Comics',
    '-Marvel Comics',
    '-Marvel Comics',
    '-DC Comics'
  ];
  static List<String> habChar_1 = [
    '-Grito hipersonico',
    '-Piroquinesis',
    '-Puede deformar la realidad',
    '-Súper fuerza',
    '-Sentido Arácnido',
    '-Experto en artes marciales',
    '-Factor de curación',
    '-Inmortal',
    '-Teletransportación',
    '-Super fuerza',
    '-Sentido Arácnido',
    '-Mayor vida del humano promedio'
  ];
  static List<String> habChar_2 = [
    '-Artes marciales',
    '-Teletransportación',
    '-Puede crear sus propios universos',
    '-Invulnerabilidad',
    '-Super fuerza',
    '-Capaz de controlar cualquier tipo de energía con su Chi',
    '-Sentidos súper desarrollados',
    '-Invulnerabilidad',
    '-Visión nocturna',
    '-Factor de curación',
    '-Super fuerza',
    '-Experta en artes marciales'
  ];
  static List<String> habChar_3 = [
    '-Habilidades en Armas',
    '-Conocimientos de Ocultismo',
    '-Poderes psiónicos',
    '-Teletransportación',
    '-Factor de curación',
    '-Posee un puño de hierro',
    '-Garras retráctiles',
    '-Deformación de la realidad',
    '-Agilidad y reflejos sobrehumanos',
    '-Golpes térmicos'
        '-Agilidad y reflejos sobrehumanos',
    '-Experta en ciencias y tecnología'
  ];
  static List<String> logrosChar = [
    '-Entrenada por la liga de asesinos',
    '-Robó el tridente de Mefistófeles ',
    '-Los celestiales lo respetan y temen',
    '-Poseyó el cuerpo de un celestial',
    '-Vence a los seis siniestros de su universo',
    '-Es el único que ha logrado noquear a Luke Cage',
    '-Salvó a todos los mutantes de los centinelas',
    '-Inició la rebelión contra Dios',
    '-Un tiempo fue co-lider de los X-Men',
    '-Asesinó a Abominación ',
    '-Investigar grietas en el continuo espacio-tiempo',
    '-Líder de la liga de asesinos'
  ];
}
