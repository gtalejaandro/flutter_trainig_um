import 'package:flutter/material.dart';

class ScreenDos extends StatefulWidget {
  ScreenDos({this.textv, this.switchv});

  final bool switchv;
  final int textv;

  @override
  ScreenDosState createState() =>
      ScreenDosState(switchv: switchv, textv: textv);
}

class ScreenDosState extends State<ScreenDos> {
  ScreenDosState({this.switchv, this.textv});

  final bool switchv;
  final int textv;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text(
            'FASE SSJ2',
            style: TextStyle(
              fontFamily: 'Anton',
              fontSize: 30,
              color: Colors.red,
              letterSpacing: 4,
            ),
          ),
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            'El valor del switch es: $switchv\nEl valor del campo de texto es: $textv',
            style: TextStyle(
              fontSize: 25,
              fontFamily: 'Teko',
              color: Colors.white70,
            ),
            textAlign: TextAlign.center,
          )
        ],
      ),
    );
  }
}
