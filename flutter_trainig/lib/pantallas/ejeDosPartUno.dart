import 'package:flutter/material.dart';
import 'ejeTresPartUno.dart';

class EjeDosPartUno extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Ejercicio #2 parte 1',
          style: TextStyle(
            fontSize: 16,
            letterSpacing: 3,
          ),
        ),
      ),
      body: ListView(
        children: [
          Row(
            // crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              CustomCard(
                color: Colors.deepPurpleAccent,
                alto: 175.0,
                ancho: 175.0,
                hijo: Icon(Icons.delete),
              ),
              CustomCard(
                color: Colors.purpleAccent,
                alto: 175.0,
                ancho: 175.0,
                hijo: Icon(Icons.save),
              )
            ],
          ),
          Divider(),
          Row(
            // crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              CustomCard(
                color: Colors.orange,
                alto: 175.0,
                ancho: 175.0,
                hijo: Icon(Icons.tab),
              ),
              CustomCard(
                color: Colors.greenAccent,
                alto: 175.0,
                ancho: 175.0,
                hijo: Icon(Icons.account_balance),
              )
            ],
          ),
          Divider(),
          Row(
            // crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              CustomCard(
                color: Colors.green,
                alto: 175.0,
                ancho: 175.0,
                hijo: Icon(Icons.headset_mic),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => EjeTresPartUno()));
                },
                child: CustomCard(
                  color: Colors.cyanAccent,
                  alto: 175.0,
                  ancho: 175.0,
                  hijo: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.fast_forward,
                        size: 55,
                        color: Colors.black,
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(30.0, 0.0, 0.0, 0.0),
                        child: Text(
                          'Pulsa Para ir a la siguiente pantalla',
                          style: TextStyle(color: Colors.black87),
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
          Divider(),
          Row(
            // crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              CustomCard(
                color: Colors.cyan,
                alto: 175.0,
                ancho: 175.0,
                hijo: Icon(Icons.battery_unknown),
              ),
              CustomCard(
                color: Colors.blueGrey,
                alto: 175.0,
                ancho: 175.0,
                hijo: Icon(Icons.help_outline),
              )
            ],
          ),
        ],
      ),
    );
  }
}

class CustomCard extends StatelessWidget {
  CustomCard({this.color, this.hijo, this.alto, this.ancho});

  final double ancho;
  final double alto;
  final Color color;
  final Widget hijo;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(15.0),
      decoration: BoxDecoration(
          color: color, borderRadius: BorderRadius.circular(10.5)),
      width: ancho,
      height: alto,
      child: hijo,
    );
  }
}
