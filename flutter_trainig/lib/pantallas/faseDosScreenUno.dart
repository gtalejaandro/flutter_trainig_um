import 'package:flutter/material.dart';
import 'faseDosScreenDos.dart';

class EjeUnoPartDos extends StatefulWidget {
  @override
  EstadoEjeUnoPartDos createState() => EstadoEjeUnoPartDos();
}

class EstadoEjeUnoPartDos extends State<EjeUnoPartDos> {
  bool switchvalue = true;
  int textfilevalue;
  TextEditingController _editingController = TextEditingController();

  void control(bool value) {
    if (switchvalue) {
      setState(() {
        switchvalue = false;
      });
    } else {
      setState(() {
        switchvalue = true;
      });
    }
  }

  void textField(String value) {
    if ((textfilevalue > 11)) {
      showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: Text(
                'YOU SHALL NOT PASS',
                style: TextStyle(
                    fontFamily: 'Teko', fontSize: 29, color: Colors.red),
                textAlign: TextAlign.center,
              ),
              content: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'El valor introducido no es valido. vuelve a intentarlo',
                    style: TextStyle(
                        fontFamily: 'Anton', fontSize: 19, color: Colors.black),
                    textAlign: TextAlign.center,
                  ),
                  RaisedButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    color: Colors.blue,
                    child: Text(
                      'ACEPTAR',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 11, fontFamily: 'Teko'),
                    ),
                  )
                ],
              ),
            );
          });
      setState(() {
        textfilevalue = 0;
      });
    } else {
      setState(() {
        textfilevalue = int.parse(_editingController.text);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white12,
      appBar: AppBar(
        title: Center(
          child: Text(
            'FASE SSJ2',
            style: TextStyle(
              fontFamily: 'Anton',
              fontSize: 30,
              color: Colors.red,
              letterSpacing: 4,
            ),
          ),
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Center(
            child: SizedBox(
              width: 300,
              child: TextField(
                controller: _editingController,
                decoration: InputDecoration(
                    labelText: 'Solo se permiten numeros entre el 0 y el 11',
                    hintText: '# entre 0 y 11'),
                keyboardType: TextInputType.number,
                onSubmitted: textField,
              ),
            ),
          ),
          Switch(
            value: switchvalue,
            onChanged: control,
            activeColor: Colors.green,
            inactiveThumbColor: Colors.red,
          ),
          SizedBox(
            height: 10,
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          print(
              'Valor de switch : $switchvalue \n Valor del textfield: $textfilevalue');

          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => ScreenDos(
                        textv: textfilevalue,
                        switchv: switchvalue,
                      )));
        },
        child: Icon(Icons.verified_user),
      ),
    );
  }
}
