import 'package:flutter/material.dart';
import 'datos.dart';

//TODO: Midifique si es neceario, la clase para que se manejen correctamente los estados requeridos.
class Resultado extends StatefulWidget {
  final bool s;
  final int t;
  Resultado(this.s, this.t);
  @override
  _ResultadoState createState() => _ResultadoState(s, t);
}

class _ResultadoState extends State<Resultado> {
  final bool s;
  final int t;
  _ResultadoState(this.s, this.t);

  // String url = Datos.imgBank[t];
  Widget formatText(String data) {
    return Text(
      '$data',
      style: TextStyle(fontSize: 16),
      textAlign: TextAlign.justify,
    );
  }

  Widget imgContainer(BuildContext context, String url) {
    double _width = MediaQuery.of(context).size.width;
    double _height = MediaQuery.of(context).size.height;

    return Container(
      width: (_width < _height) ? _width / 1.765 : _height / 1.765,
      height: (_width < _height) ? _width / 1.765 : _height / 1.765,
      margin: EdgeInsets.only(top: 10),
      decoration: BoxDecoration(
          image: DecorationImage(image: AssetImage(url)),
          borderRadius: BorderRadius.circular(20),
          border: Border.all(color: Colors.blue, width: 5.0)),
    );
  }

  Widget cardInfo(
      BuildContext context,
      String nombre,
      String procedencia,
      String habilidad1,
      String habilidad2,
      String habilidad3,
      String moreinfo) {
    double _width = MediaQuery.of(context).size.width;
    double _height = MediaQuery.of(context).size.height;

    return Container(
      width: (_width < _height) ? _width / 1.765 : _height / 1.765,
      margin: EdgeInsets.only(top: 10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          border: Border.all(color: Colors.blue, width: 5.0)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          formatText(nombre),
          formatText(procedencia),
          formatText(habilidad1),
          formatText(habilidad2),
          formatText(habilidad3),
          formatText(moreinfo),
        ],
      ),
    );
  }

  Widget cardSimple(BuildContext context, String nombre, String procedencia) {
    double _width = MediaQuery.of(context).size.width;
    double _height = MediaQuery.of(context).size.height;
    return Container(
      width: (_width < _height) ? _width / 1.765 : _height / 1.765,
      margin: EdgeInsets.only(top: 10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          border: Border.all(color: Colors.blue, width: 5.0)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          formatText(nombre),
          formatText(procedencia),
        ],
      ),
    );
  }

  Widget hideWidgetButtom(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width / 4,
      height: MediaQuery.of(context).size.height / 13.33,
      margin: EdgeInsets.only(top: 10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          border: Border.all(color: Colors.blue, width: 5.0)),
      child: FlatButton(child: Text('Hide'), onPressed: null),
    );
  }

  Widget popButtom(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width / 4,
      height: MediaQuery.of(context).size.height / 13.33,
      margin: EdgeInsets.only(top: 10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          border: Border.all(color: Colors.blue, width: 5.0)),
      child: FlatButton(child: Text('Exit'), onPressed: null),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Resultado'),
      ),
      body: ListView(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              //TODO: consultar y agregar la ruta de la image que se desea ver
              imgContainer(context, ''),
            ],
          ),
          //TODO: Complemente esta seccion para mostrar el CARD con los datos, segun lo especificado en el documento
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[hideWidgetButtom(context), popButtom(context)],
          )
        ],
      ),
    );
  }
}
